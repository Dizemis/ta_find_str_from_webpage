import html2text
from finite_automate import *
import optparse


def url_to_text(url):
    text_maker = html2text.HTML2Text()
    text_maker.ignore_links = True
    text_maker.ignore_images = True
    encoding = 'utf-8'
    data = html2text.urllib.urlopen(url).read().decode(encoding)
    text = text_maker.handle(data)
    return text


if __name__ == '__main__':
    p = optparse.OptionParser('%prog [(filename|url)]')
    args = p.parse_args()[1]
    if len(args) == 0:
        url = input('введите url or file path: ')
    else:
        url = args[0]
    #url = 'https://habr.com/sandbox/35982/'
    if url.startswith('http://') or url.startswith(('https://')):
        text = url_to_text(url)
    else:
        text = open(url, 'r').read()
    print(text)
    find_str = input('Введите искомую строку: ')
    occurrences = finite_automation_matcher(text, compute_transition_function(find_str), len(find_str))
    print('Колво вхождений = %d' % len(occurrences))
