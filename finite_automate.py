def finite_automation_matcher(text, transition_function, len_of_str, all_index=True, letter_case=False):
    n = len(text)
    q = 0
    if all_index:
        arr_of_index = []
    for i in range(0, n):
        # зная текущее состояние и подаваемый символ - переходим к следующему
        # состоянию, используя функцию перехода
        # в данном случае словарь вида (q_, sim) = q_i+1
        if letter_case:
            try:
                q = transition_function[(q, text[i])]
            except KeyError:
                q = 0
        else:
            try:
                q1 = transition_function[(q, text[i].lower())]
            except KeyError:
                q1 = 0
            try:
                q2 = transition_function[(q, text[i].upper())]
            except KeyError:
                q2 = 0
            q = max(q1, q2)
        if q == len_of_str:
            if all_index:
                arr_of_index.append(i - len_of_str + 1)
            else:
                return i - len_of_str + 1
    if all_index:
        return arr_of_index
    else:
        return None


def compute_transition_function(find_str):
    alphabet = set(find_str)
    len_of_str = len(find_str)
    transition_function = {}
    # записываем функцию перехода для каждого символа переданного каждому состоянию
    for q in range(0, len_of_str + 1):
        for simvol in alphabet:
            k = min(len_of_str, q + 1)
            while find_str[:k] != (find_str[:q] + simvol)[q + 1 - k:]:
                k -= 1
            transition_function[(q, simvol)] = k
    return transition_function


if __name__ == '__main__':  # тестирование
    print(finite_automation_matcher('The~ following pRocedure computes the transition function',
                                    compute_transition_function('pro'),
                                    3))

    def test_transition_function(transition_function, len_of_str):
        arr_of_trans = [[] for i in range(len_of_str + 1)]
        for i in transition_function:
            for j in range(len_of_str + 1):
                if transition_function[i] == j:
                    arr_of_trans[j].append(i)
        for j in range(len_of_str + 1):
            print("for q%s:" % str(j), end=' ')
            for k in arr_of_trans[j]:
                print(k, end=' ')
            print()
    test_transition_function(compute_transition_function('pro'), 3)
    test_transition_function(compute_transition_function('ABAAB'), 5)
